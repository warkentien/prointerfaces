package P9_23;

/**
 * Created by sethf_000 on 10/25/2015.
 *
 * This class sets description and date inforation for an appointment. It is a superclass
 * of Daily, Monthly and Onetime.
 */
public class Appointment {
    private String description;
    private int year;
    private int month;
    private int day;

    public Appointment(){

    }

    public Appointment(String description, int year, int month, int day){
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public String getDescription(){
        return this.description;
    }
    public int getYear(){
        return this.year;
    }
    public int getMonth(){
        return this.month;
    }
    public int getDay(){
        return this.day;
    }

    public boolean occursOn(int year, int month, int day){

        if (this instanceof Monthly) {

        }
        if (this instanceof Daily) {

        }
        if (this instanceof Onetime) {

        }

        return true;
    }

}
