package P9_23;

/**
 * Created by sethf_000 on 10/25/2015.
 *
 * This class is a subtype of the Appointment superclass. We can use it to specify an
 * appointment as repeating Daily
 */
public class Daily extends Appointment {
    private String description;
    private int year;
    private int month;
    private int day;

    public Daily(String description, int year, int month, int day){
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public String getDescription(){
        return this.description;
    }
    public int getYear(){
        return this.year;
    }
    public int getMonth(){
        return this.month;
    }
    public int getDay(){
        return this.day;
    }

}
