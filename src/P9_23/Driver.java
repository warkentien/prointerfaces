package P9_23;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Created by sethf_000 on 10/25/2015.
 *
 * This class allows a user to enter an appointment type (Onetime/Daily/Monthly), a description
 * of the appointment and a date for the appointment. It then saves the appointment information
 * to a file, which the user names.
 * The user is then given a choice of loading an appointment's information from file by entering
 * the filename where the information was stored. The available files are listed for the user
 * to choose from.
 */
public class Driver {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        //get the appointment information from the user
        System.out.print("Enter the type of appointment (Onetime, Daily or Monthly): ");
        String appointment =  scan.nextLine();
        System.out.print("Enter the description of the appointment: ");
        String description =  scan.nextLine();
        System.out.print("Enter the year of the appointment: ");
        int year =  scan.nextInt();
        System.out.print("Enter the numerical month for the appointment: ");
        int month =  scan.nextInt();
        System.out.print("Enter the numerical day of the appointment: ");
        int day =  scan.nextInt();

        appointment = appointment.toLowerCase();

        //determine the appointment type and create an object of that type with the entered information
        if (appointment.equals("monthly")){
            Appointment newAppointment = new Monthly(description, year, month, day);
            save(newAppointment);
        }
        else if (appointment.equals("onetime")){
            Appointment newAppointment = new Onetime(description, year, month, day);
            save(newAppointment);
        }
        else if (appointment.equals("daily")){
            Appointment newAppointment = new Daily(description, year, month, day);
            save(newAppointment);
        }
        else {
            System.out.println("That is not a valid appointment type.");
        }
        scan.nextLine();


        //after saving the entered information, see if the user wants to load
        //any information from a file
        System.out.println("Do you want to load a saved appointment file? (y/n): ");
        String ans = scan.nextLine();
        ans.toLowerCase();
        if(ans.equals("y")){

            //list all available files in this directory
            String path = ".";
            String files;
            File folder = new File(path);
            File[] listOfFiles = folder.listFiles();
            System.out.println("Enter one of the file names listed below: ");

            for (int i = 0; i < listOfFiles.length; i++)
            {

                if (listOfFiles[i].isFile())
                {
                    files = listOfFiles[i].getName();
                    System.out.println(files);
                }
            }
            //get file name from user
            System.out.println("Enter file name: ");
            String fileName =  scan.nextLine();
            load(fileName);
        }

        else {
            System.out.println("Goodbye!");
        }


    }

    //save the appointment information from the created object
    public static void save(Appointment newAppointment){
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the filename to save the information to: ");
        String file =  in.next();

        try{
            PrintStream out = new PrintStream(file);
            out.println("Type of appointment: " + newAppointment.getClass());
            out.println("Description: " + newAppointment.getDescription());
            out.println("Date: " + newAppointment.getMonth() + "/" + newAppointment.getDay() + "/" +
                    newAppointment.getYear());
            out.close();
            System.out.println();
            System.out.println("Your information was successfully saved!");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    //load the information from file
    public static void load(String fileName){
        File file = new File(fileName);
        try {
            Scanner inFile = new Scanner(file);

            System.out.println("Here is your appointment information: ");
            while (inFile.hasNext()){
                System.out.println(inFile.nextLine());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }



}

