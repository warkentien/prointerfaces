package P9_10;

import java.awt.*;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * This class extends the regular Java Rectangle class, adding methods for area and perimeter.
 * It doesn't use any fields, just calls the setLocation and setSize methods of the super class
 * to set the dimensions and locations of the rectangle.
 */
public class BetterRectangle extends Rectangle {


    public BetterRectangle(int x, int y, int height, int width){
        setLocation(x, y);
        setSize(width, height);
    }

    public double getPerimeter(){
        return (this.height * 2) + (this.width * 2);
    }

    public double getArea(){
        return (this.height * this.width);
    }


}
