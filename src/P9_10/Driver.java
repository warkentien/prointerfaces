package P9_10;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * This Driver class tests the BetterRectangle class, which extends the built in
 * Java Rectangle class and adds methods for calculating the perimeter and area
 */
public class Driver {
    public static void main(String[] args) {

        BetterRectangle rect = new BetterRectangle(0, 0, 23, 41);

        System.out.println("Perimeter: " + rect.getPerimeter());
        System.out.println("Area: " + rect.getArea());

    }
}
