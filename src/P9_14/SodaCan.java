package P9_14;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * This class has a method to compute the surface area of a soda can. It implements the
 * Measurable interface to it can compute the average of multiple can surface areas.
 */
public class SodaCan implements Measurable {

    //declare the fields for this class
    private double height;
    private double radius;

    //constructor that sets the height and radius on object creation
    public SodaCan(double height, double radius){
        this.height = height;
        this.radius = radius;
    }

    //method that computes the surface area using formula for surface area of a cylindar
    public double getSurfaceArea(){
        return (2.0 * radius * Math.PI * height) +
                (2.0 * Math.PI * radius * radius);
    }

    public double getMeasure()
    {
        return getSurfaceArea();
    }

}
