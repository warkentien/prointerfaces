package P9_14;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * This Driver class uses the SodaCan class and the Measurable interface to set the dimensions
 * of an array of can objects and then compute the average surface area of those cans.
 *
 */
public class Driver {
    public static void main(String[] args) {

        Measurable[] cans = new Measurable[3];
        cans[0] = new SodaCan(10, 3);
        cans[1] = new SodaCan(20, 15);
        cans[2] = new SodaCan(8, 4);

        System.out.println("Average surface area: " + average(cans));

    }

    public static double average(Measurable[] objects) {
        if (objects.length == 0) { return 0; }
        double sum = 0;
        for (Measurable obj : objects)
            {
            sum = sum + obj.getMeasure();
            }
        return sum / objects.length;
        }

}
