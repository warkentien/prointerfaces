package P9_13;

import java.awt.*;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * This Driver class creates a LabeledPoint object, setting it's x and y coordinates
 * as well as the name of its location. It then calls the class' toString method to
 * print the information
 */
public class Driver {

    public static void main(String[] args) {

        LabeledPoint point = new LabeledPoint(20, 40, "target");

        System.out.println(point);

    }

}
