package P9_13;

import java.awt.*;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * This class creates a new Point object, which we use to set the x and y coordinates of
 * the point. This class also includes a field for the name of the location.
 * The toString method of this class includes information about the label, then calls
 * the superclass to print the x and y coordinates.
 */
public class LabeledPoint extends Point{

    private String label;
    private Point point = new Point();

    public LabeledPoint(int x, int y, String label) {

        point.setLocation(x, y);
        this.label = label;
    }

    public String toString() {
        return "The location of " + label + " is " + point;
    }

}
