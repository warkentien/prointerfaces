package P9_8;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * Subclass of Person. In addition to the fields it uses from Person, it will also set major.
 */
public class Student extends Person {

    private String major;

    //the constructor will set name and birthyear using the Person superclass
    public Student(String name, int birthYear, String major){
        super(name, birthYear);
        this.major = major;
    }

    public String getmajor(){
        return this.major;
    }

    @Override
    public String toString(){
        return super.toString() + "\nStudent's major: " + getmajor();
    }

}
