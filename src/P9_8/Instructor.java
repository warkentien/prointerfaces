package P9_8;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * Subclass of Person. In addition to the fields it uses from Person, it will also set salary.
 */
public class Instructor extends Person {

    private int salary;

    //the constructor will set name and birthyear using the Person superclass
    public Instructor(String name, int birthYear, int salary){
        super(name, birthYear);
        this.salary = salary;
    }

    public int getSalary(){
        return this.salary;
    }

    @Override
    public String toString(){
        return super.toString() + "\nInstructor's salary: $" + getSalary();
    }


}
