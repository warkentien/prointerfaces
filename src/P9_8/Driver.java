package P9_8;

/**
 * Created by sethf_000
 * on 10/24/2015.
 *
 * This class uses the Person, Student and Instructor classes to print the information.
 * Each class has an overridden toString method to print the person's info.
 * Student and Instructor both inheret from Person and set the name and birthYear variables using the
 * Person superclass.
 */
public class Driver {

    public static void main(String[] args) {

        //create new Person object with name and birth year
        Person person = new Person("Frank Charles", 1950);
        //create new Student object with name, birth year and major
        Student student = new Student("Jim Sanders", 1994, "Math");
        //create new Instructor object with name, birth year and salary
        Instructor instructor = new Instructor("Bob Cromwell", 1961, 80000);

        //call the toString methods for each object
        System.out.println(person);
        System.out.println();
        System.out.println(student);
        System.out.println();
        System.out.println(instructor);

    }
}
