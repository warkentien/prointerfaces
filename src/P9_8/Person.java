package P9_8;

/**
 * Created by sethf_000 on 10/24/2015.
 *
 * Superclass of Student and Instructor. We can set name and birth year here for any
 * of the three classes and print the person's name and bith year.
 */
public class Person {

    // set the fields
    private String name;
    private int birthYear;

    //default constructor
    public Person(){
    }

    //constructor takes in name and birthyear
    public Person(String name, int birthYear){
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName(){
        return this.name;
    }

    public int getbirthYear(){
        return this.birthYear;
    }

    @Override
    public String toString(){
        return "Name: " + getName() + "\nBirth Year: " + getbirthYear();
    }


}
