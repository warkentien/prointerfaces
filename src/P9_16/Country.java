package P9_16;

/**
 * Created by sethf_000 on 10/25/2015.
 *
 * This class implements the Measurable interface to
 * determine the country with the largest area.
 *
 */
public class Country implements Measurable{
    //set the fields
    private String name;
    private double area;

    public Country(String name, double area){
        this.name = name;
        this.area = area;
    }

    public String getName()
    {
        return name;
    }

    //getMeasure method for the Measurable interface
    public double getMeasure()
    {
        return area;
    }

    public String toString(){
        return this.getName();
    }

}
