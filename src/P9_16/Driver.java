package P9_16;


/**
 * Created by sethf_000 on 10/25/2015.
 *
 *  This Driver class uses the Country class and the Measurable interface to set the name and
 * area of an array of country objects and then determine which has the largest area.
 */
public class Driver {
    public static void main(String[] args) {
        Measurable[] countries = new Measurable[3];
        countries[0] = new Country("France", 247368);
        countries[1] = new Country("UK", 93628);
        countries[2] = new Country("U.S.", 3794100);

        System.out.println("The largest country is: " + maximum(countries));
    }

    public static Measurable maximum(Measurable[] objects){
        Measurable largest = objects[0];
        for (Measurable obj : objects) {
            if (obj.getMeasure() > largest.getMeasure()){
                largest = obj;
            }
        }
        return largest;
    }


}
