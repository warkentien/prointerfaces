package P9_16;

/**
 * Created by sethf_000 on 10/25/2015.
 *
 * the Measurable interface requires the getMeasure method to be included in an implementation
 */
public interface Measurable {

    double getMeasure();

}
